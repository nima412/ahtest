﻿using System;
using System.Collections.Generic;
using System.Text;
using AhTask.Domain.Entities.Enums;
using AhTask.ViewModel.Common;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace AhTask.ViewModel.Property
{
    public class PropertyViewModel : BaseViewModel
    {
        public string Name { get; set; }

        public decimal Area { get; set; }
        public string Address { get; set; }

        public Directions Direction { get; set; }
        public string Description { get; set; }

        public int OwnerId { get; set; }
        public string OwnerFirstName { get; set; }
    }
}
