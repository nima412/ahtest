﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AhTask.Domain.DAL;
using AhTask.Domain.Entities;
using AhTask.Service.Contracts;
using AhTask.ViewModel.Property;
using AutoMapper;

namespace AhTask.Web.Controllers
{
    public class PropertiesController : Controller
    {
        private readonly IOwnerRepository _ownerRepository;
        private readonly IPropertyRepository _propertyRepository;
        private readonly IMapper _mapper;
        public PropertiesController(IPropertyRepository propertyRepository, IOwnerRepository ownerRepository, IMapper mapper)
        {
            _propertyRepository = propertyRepository;
            _ownerRepository = ownerRepository;
            _mapper = mapper;
        }

        // GET: Properties
        public async Task<IActionResult> Index()
        {

            return View(_mapper.Map<List<PropertyViewModel>>(await _propertyRepository.GetAsync()));
        }



        // GET: Properties/Create
        public async Task<IActionResult> Create()
        {
            var model = new PropertyAddViewModel()
            {
                Owners = _mapper.Map<List<SelectListItem>>(await _ownerRepository.GetAsync())
            };
            return View(model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(PropertyAddViewModel vm)
        {
            if (ModelState.IsValid)
            {
                await _propertyRepository.AddAsync(_mapper.Map<Property>(vm));
                await _propertyRepository.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            return View(vm);
        }

        // GET: Properties/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vm = _mapper.Map<PropertyEditViewModel>(await _propertyRepository.FindByIdAsync(id.Value));
            if (vm == null)
            {
                return NotFound();
            }

            vm.Owners = _mapper.Map<List<SelectListItem>>(await _ownerRepository.GetAsync());
            return View(vm);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(PropertyEditViewModel vm)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    await _propertyRepository.EditAsync(_mapper.Map<Property>(vm));
                    await _propertyRepository.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException exception)
                {
                    ModelState.AddModelError("", exception.Message);
                }
                return RedirectToAction(nameof(Index));
            }
            return View(vm);
        }

        // GET: Properties/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vm = _mapper.Map<PropertyViewModel>(await _propertyRepository.FindByIdAsync(id.Value));
            if (vm == null)
            {
                return NotFound();
            }

            return View(vm);
        }

        // POST: Properties/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _propertyRepository.DeleteAsync(id);
            await _propertyRepository.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

    }
}
