﻿using AhTask.Domain.Entities;
using AhTask.ViewModel.Property;
using AutoMapper;

namespace AhTask.AutoMapper
{
    public class PropertyProfile : Profile
    {
        public PropertyProfile()
        {

            CreateMap<PropertyAddViewModel, Property>();
            CreateMap<Property, PropertyEditViewModel>();
            CreateMap<PropertyEditViewModel, Property>();
            CreateMap<Property, PropertyViewModel>();
        }
    }
}
