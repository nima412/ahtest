﻿using AhTask.Domain.Entities;
using AutoMapper;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace AhTask.AutoMapper
{
    public class OwnerProfile : Profile
    {
        public OwnerProfile()
        {
            CreateMap<Owner, SelectListItem>()
                .ForMember(p => p.Text, p => p.MapFrom(q => q.FirstName +" " +q.LastName))
                .ForMember(p => p.Value, p => p.MapFrom(q => q.Id));

            //CreateMap<OwnerAddViewModel, Owner>();
            //CreateMap<Owner, OwnerEditViewModel>();
            //CreateMap<OwnerEditViewModel, Owner>();
            //CreateMap<Owner, OwnerViewModel>();
        }
    }
}