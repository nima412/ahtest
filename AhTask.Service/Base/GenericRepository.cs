﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using AhTask.Domain.DAL;
using AhTask.Domain.Entities.Base;
using Microsoft.EntityFrameworkCore;

namespace AhTask.Service.Base
{

    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : BaseEntity
    {
        public DbSet<TEntity> Set;
        private readonly ApplicationDbContext _context;
        public GenericRepository(ApplicationDbContext context)
        {
            _context = context;
            Set = context.Set<TEntity>();
        }
        public async Task<IQueryable<TEntity>> GetAsync(Expression<Func<TEntity, bool>> filter = null)
        {
            if (filter == null) return Set.Where(x=>!x.IsDeleted).AsQueryable();
            return Set.Where(filter);
        }

        public async Task<TEntity> FindByIdAsync(int id)
        {
            return await Set.SingleOrDefaultAsync(x => x.Id == id);
        }

        public async Task<TEntity> EditAsync(TEntity entity)
        {
            Set.Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
            return entity;
        }

        public async Task<TEntity> AddAsync(TEntity entity)
        {
            await Set.AddAsync(entity);
            return entity;
        }

        public async Task DeleteAsync(int id, bool softDelete = true)
        {
            var item = await FindByIdAsync(id);
            if (_context.Entry(item).State == EntityState.Detached)
            {
                Set.Attach(item);
            }

            if (softDelete)
            {
                item.IsDeleted = true;
                await EditAsync(item);
                return;
            }

            Set.Remove(item);
        }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
