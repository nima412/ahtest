﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using AhTask.Domain.Entities.Base;
using Microsoft.EntityFrameworkCore;

namespace AhTask.Service.Base
{
    
    public interface IGenericRepository<TEntity> where TEntity : BaseEntity
    {
      
        Task<IQueryable<TEntity>> GetAsync(Expression<Func<TEntity, bool>> filter = null);
        Task<TEntity> FindByIdAsync(int id);
        Task<TEntity> EditAsync(TEntity entity);
        Task<TEntity> AddAsync(TEntity entity);
        Task DeleteAsync(int id,bool softDelete = true);

        Task SaveChangesAsync();



    }
}
