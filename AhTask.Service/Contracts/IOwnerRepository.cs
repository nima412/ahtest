﻿using AhTask.Domain.Entities;
using AhTask.Service.Base;

namespace AhTask.Service.Contracts
{
    public interface IOwnerRepository : IGenericRepository<Owner>
    {
    }
}