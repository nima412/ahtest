﻿using System;
using System.Collections.Generic;
using System.Text;
using AhTask.Domain.Entities;
using AhTask.Service.Base;

namespace AhTask.Service.Contracts
{
    public interface IPropertyRepository : IGenericRepository<Property>
    {
    }
}
