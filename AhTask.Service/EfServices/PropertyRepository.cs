﻿using System;
using System.Collections.Generic;
using System.Text;
using AhTask.Domain.DAL;
using AhTask.Domain.Entities;
using AhTask.Service.Base;
using AhTask.Service.Contracts;

namespace AhTask.Service.EfServices
{
    public class PropertyRepository : GenericRepository<Property> ,IPropertyRepository
    {
        public PropertyRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}
