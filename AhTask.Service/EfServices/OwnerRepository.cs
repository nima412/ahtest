﻿using AhTask.Domain.DAL;
using AhTask.Domain.Entities;
using AhTask.Service.Base;
using AhTask.Service.Contracts;

namespace AhTask.Service.EfServices
{
    public class OwnerRepository : GenericRepository<Owner> , IOwnerRepository
    {
        public OwnerRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}