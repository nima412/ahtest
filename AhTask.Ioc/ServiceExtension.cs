﻿using AhTask.Service.Contracts;
using AhTask.Service.EfServices;
using Microsoft.Extensions.DependencyInjection;

namespace AhTask.Ioc
{
    public static class ServiceExtension
    {
        public static IServiceCollection AddServices(this IServiceCollection service)
        {
            service.AddScoped<IOwnerRepository, OwnerRepository>();
            service.AddScoped<IPropertyRepository, PropertyRepository>();

            return service;
        }
    }
}
