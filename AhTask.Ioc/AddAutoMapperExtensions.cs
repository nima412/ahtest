﻿using System;
using System.Linq;
using AhTask.AutoMapper;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;

namespace AhTask.Ioc
{
    public static class AddAutoMapperExtensions
    {
        public static void AddAutoMapper(this IServiceCollection services)
        {
            var profiles =
                from t in typeof(PropertyProfile).Assembly.GetTypes()
                where typeof(Profile).IsAssignableFrom(t)
                select (Profile)Activator.CreateInstance(t);

            var config = new MapperConfiguration(cfg =>
            {
                foreach (var profile in profiles)
                {
                    cfg.AddProfile(profile);
                }
            });


            services.AddScoped<IMapper>(factory => factory.GetService<MapperConfiguration>().CreateMapper());
            services.AddScoped<MapperConfiguration>(factory => config);
        }
    }
}