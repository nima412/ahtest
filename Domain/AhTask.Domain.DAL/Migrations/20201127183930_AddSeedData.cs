﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AhTask.Domain.DAL.Migrations
{
    public partial class AddSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Owners",
                columns: new[] { "OwnerID", "CreatedByUserId", "CreatedOn", "FirstName", "IsDeleted", "LastName", "ModifiedByUserId", "ModifiedOn", "PhoneNumber" },
                values: new object[] { 1, "", new DateTime(2020, 11, 27, 22, 9, 29, 678, DateTimeKind.Local).AddTicks(2035), "نیما", false, "طالبی", "", new DateTime(2020, 11, 27, 22, 9, 29, 680, DateTimeKind.Local).AddTicks(9325), "09337547905" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Owners",
                keyColumn: "OwnerID",
                keyValue: 1);
        }
    }
}
