﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using AhTask.Domain.Entities;
using AhTask.Domain.Entities.Config;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace AhTask.Domain.DAL
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Apply all configurations
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(OwnerConfig).Assembly);
            
            //add Default data 
            modelBuilder.Entity<Owner>().HasData(new Owner()
            {
                FirstName = "نیما",
                LastName = "طالبی",
                CreatedOn = DateTime.Now,
                IsDeleted = false,
                PhoneNumber = "09337547905",
                ModifiedOn = DateTime.Now,
                Id = 1,
                CreatedByUserId = "",
                ModifiedByUserId = "",
            });

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Property> Properties { get; set; }
        public DbSet<Owner> Owners { get; set; }
    }
}
