﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AhTask.Domain.Entities.Base
{
    public class BaseEntity
    {
        public int Id { get; set; }
        public bool IsDeleted { get; set; }

        public DateTime CreatedOn { get; set; }
        public string CreatedByUserId { get; set; }

        public DateTime ModifiedOn { get; set; }
        public string ModifiedByUserId { get; set; }
    }
}
