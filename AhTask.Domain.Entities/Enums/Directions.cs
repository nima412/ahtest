﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AhTask.Domain.Entities.Enums
{
    public enum Directions
    {
        [Display(Name = "شمالی")]
        North,
        [Display(Name = "جنوبی")]
        South
    }
}
