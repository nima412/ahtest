﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AhTask.Domain.Entities.Config
{
    public class PropertyConfig : IEntityTypeConfiguration<Property>
    {
        public void Configure(EntityTypeBuilder<Property> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).HasColumnName("PropertyID");
            builder.HasOne(x => x.Owner).WithMany(x => x.Properties).HasForeignKey(x => x.OwnerId);
        }
    }
}
