﻿using System;
using System.Collections.Generic;
using System.Text;
using AhTask.Domain.Entities.Base;

namespace AhTask.Domain.Entities
{
    public class Owner : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }

        public ICollection<Property> Properties { get; set; }
    }
}
