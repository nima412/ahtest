﻿using System;
using System.Collections.Generic;
using System.Text;
using AhTask.Domain.Entities.Base;
using AhTask.Domain.Entities.Enums;

namespace AhTask.Domain.Entities
{
    public class Property : BaseEntity
    {
        public string Name { get; set; }

        public decimal Area { get; set; }
        public string Address { get; set; }

        public Directions Direction { get; set; }
        public string Description { get; set; }

        public int OwnerId { get; set; }
        public Owner Owner { get; set; }

    }
}
